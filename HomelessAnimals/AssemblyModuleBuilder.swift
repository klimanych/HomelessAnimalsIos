//
//  ModuleBuilder.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import UIKit
import YandexMapsMobile


protocol AssemblyBuilderProtocol {
    
    func createLaunchSreenModule(router: RouterProtocol) -> UIViewController
    func createAuthModule(router: RouterProtocol) -> UIViewController
    func createMapModule(router: RouterProtocol) -> UIViewController
    func createDetailModule(router: RouterProtocol, pet: Pet?, coordinates: YMKPoint?) -> UIViewController
    func createMenuModule(router: RouterProtocol, panelTransition: PanelTransition) -> UIViewController
    func createDevicesModule(router: RouterProtocol, panelTransition: PanelTransition) -> UIViewController
//    func createNavigationMenuModule(router: RouterProtocol, panelTransition: PanelTransition) -> UINavigationController
    
}

class AssemblyModuleBuilder: AssemblyBuilderProtocol {
    
    var localDataService: LocalDataServiceProtocol
    var networkService: NetworkServiceProtocol
    
    init() {
        self.localDataService = LocalDataService()
        self.networkService = NetworkService(localDataService: self.localDataService, deviceName: UIDevice.current.name+" "+UIDevice.current.model)
    }
    
    
    func createLaunchSreenModule(router: RouterProtocol) -> UIViewController {
        let view = LaunchScreenViewController()
        let presenter = LaunchScreenPresenter(view: view, router: router)
        view.presenter = presenter
        return view
    }
    
    func createAuthModule(router: RouterProtocol) -> UIViewController {
        let view = AuthViewController()
//        let networkService = NetworkService(localDataService: localDataService)
        let presenter = AuthPresenter(view: view, networkService: networkService, router: router, localDataService: localDataService)
        view.presenter = presenter
        return view
    }
    
    func createMapModule(router: RouterProtocol) -> UIViewController {
        let view = MapViewController()
//        let networkService = NetworkService(localDataService: localDataService)
        let presenter = MapPresenter(view: view, networkService: networkService, router: router, localDataService: localDataService)
        view.presenter = presenter
        return view
    }
    
    func createDetailModule(router: RouterProtocol , pet: Pet?, coordinates: YMKPoint?) -> UIViewController {
        let view = DetailViewController()
//        let networkService = NetworkService(localDataService: localDataService)
        let presenter = DetailPresenter(view: view, networkService: networkService, router: router, pet: pet, coordinates: coordinates, localDataService: localDataService)
        view.presenter = presenter
        return view
    }
    
    func createMenuModule(router: RouterProtocol, panelTransition: PanelTransition) -> UIViewController {
        let view = MenuViewController()
//        let networkService = NetworkService(localDataService: localDataService)
        let presenter = MenuPresenter(view: view, networkService: networkService, router: router, localDataService: localDataService)
        view.presenter = presenter
        view.customTransition = panelTransition
        return view
    }
    
    func createDevicesModule(router: RouterProtocol, panelTransition: PanelTransition) -> UIViewController {
        let view = DevicesViewController()
        let presenter = DevicesPresenter(view: view, networkService: networkService, router: router, localDataService: localDataService)
        view.presenter = presenter
        view.customTransition = panelTransition
        if let currentTokenId = localDataService.getApiKey() {
            view.currentTokenId = currentTokenId
        }
//        view.currentTokenId = localDataService.getApiKey()
        return view
    }
    
}

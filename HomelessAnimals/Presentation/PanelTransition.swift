//
//  PalenTransition.swift
//  HomelessAnimals
//
//  Created by klimanych on 23.03.2021.
//

import UIKit

class PanelTransition: NSObject, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
    
    private let driver = TransitionDriver()
    var direction  = ShowDirection.left
    var heightMultiplier : CGFloat = 1.0
    var widthMultiplier: CGFloat = 1.0
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        driver.link(to: presented)
        driver.showDirection = direction
        
        let controller = DimmPresentationController(presentedViewController: presented, presenting: presenting ?? source)
        controller.driver = driver
        controller.heightMultiplier = heightMultiplier
        controller.widthMultiplier = widthMultiplier
        controller.direction = direction
        return controller
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animation = PresentAnimation()
        animation.direction = direction
        return animation
    }
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
//        print("dismissing")
        let animation = DismissAnimation()
        animation.direction = direction
        return animation
    }
    
    // MARK: - Interaction
        func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
            return driver
        }
        
        func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
            return driver
        }
    
}




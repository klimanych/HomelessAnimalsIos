//
//  ShowDirection.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.04.2021.
//

import Foundation


enum ShowDirection {
    case left, right
}

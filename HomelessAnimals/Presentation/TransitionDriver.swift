//
//  TransitionDriver.swift
//  HomelessAnimals
//
//  Created by klimanych on 25.03.2021.
//

import UIKit

class TransitionDriver: UIPercentDrivenInteractiveTransition {
    
    
    
    // MARK: - Linking
    func link(to controller: UIViewController) {
        presentedController = controller
        
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handle(recognizer:)))
        presentedController?.view.addGestureRecognizer(panRecognizer!)
    }
    
    private weak var presentedController: UIViewController?
    private var panRecognizer: UIPanGestureRecognizer?
    var direction: TransitionDirection = .present
    var showDirection : ShowDirection = .left
    
    
    // MARK: - Override
    override var wantsInteractiveStart: Bool {
        get {
            switch direction {
            case .present:
                return false
            case .dismiss:
                let gestureIsActive = panRecognizer?.state == .began
                return gestureIsActive
            }
        }
        
        set { }
    }
    
    // MARK: - Direction

    
    @objc private func handle(recognizer r: UIPanGestureRecognizer) {
        
//        print("handling")
        
        switch direction {
        case .present:
//            print("present")
            handlePresentation(recognizer: r)
        case .dismiss:
//            print("dismiss")
            handleDismiss(recognizer: r)
        }
    }
}

// MARK: - Gesture Handling
extension TransitionDriver {
    
    private func handlePresentation(recognizer r: UIPanGestureRecognizer) {
        print("completed percentage show = \(percentComplete)")
        switch r.state {
        case .began:
            pause()
        case .changed:
            let increment = directionMultiplier * r.incrementToBottom(maxTranslation: maxTranslation)
            update(percentComplete + increment)
            
        case .ended, .cancelled:
            if r.isProjectedToDownHalf(maxTranslation: maxTranslation, showDirection: showDirection) {
                cancel()
            } else {
                finish()
            }
            
        case .failed:
            cancel()
            
        default:
            break
        }
    }
    
    private func handleDismiss(recognizer r: UIPanGestureRecognizer) {
    
        switch r.state {
        case .began:
            pause() // Pause allows to detect isRunning
            
            if !isRunning {
                presentedController?.dismiss(animated: true) // Start the new one
            }
            
        case .changed:
            update(percentComplete + directionMultiplier * r.incrementToBottom(maxTranslation: maxTranslation))
            
        case .ended, .cancelled:
//            print(percentComplete)
//            print("completed percentage on dismiss = \(percentComplete)")
            if r.isProjectedToDownHalf(maxTranslation: maxTranslation, showDirection: showDirection) || percentComplete > 0.5 {
                finish()
            } else {
                cancel()
            }
            
        case .failed:
            cancel()
            
        default:
            break
        }
    }
    
    var maxTranslation: CGFloat {
        return presentedController?.view.frame.width ?? 0
    }
    
    /// `pause()` before call `isRunning`
    private var isRunning: Bool {
        return percentComplete != 0
    }
    
    var directionMultiplier: CGFloat {
        return (showDirection == .left ? CGFloat(-1.0) : CGFloat(1.0))
    }
}

private extension UIPanGestureRecognizer {
    func isProjectedToDownHalf(maxTranslation: CGFloat, showDirection: ShowDirection) -> Bool {
        
//        print("коорды точи")
//        print(view?.frame.)
        
        let endLocation = projectedLocation(decelerationRate: .fast)
        
//        endLocation.x = endLocation.x * invertFloat * -1.0
        
        let isPresentationCompleted = (showDirection == .left) ?  endLocation.x < maxTranslation / 2 : endLocation.x > maxTranslation * 2
        
//        print("calculated ending = \(isPresentationCompleted)")
        
        return isPresentationCompleted
    }
    
    func incrementToBottom(maxTranslation: CGFloat) -> CGFloat {
        let translation = self.translation(in: view).x
//        print("translation x = \(translation)")
//        print(translation)
        setTranslation(.zero, in: nil)
        
        let percentIncrement = translation / maxTranslation
        return percentIncrement
    }
}

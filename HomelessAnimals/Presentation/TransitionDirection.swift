//
//  TransitionDirection.swift
//  HomelessAnimals
//
//  Created by klimanych on 25.03.2021.
//

import Foundation


enum TransitionDirection {
    case present, dismiss
}

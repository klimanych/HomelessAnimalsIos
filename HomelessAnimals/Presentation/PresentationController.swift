//
//  PresentationController.swift
//  HomelessAnimals
//
//  Created by klimanych on 23.03.2021.
//

import UIKit


class PresentationController: UIPresentationController {

    
    var driver: TransitionDriver!
    var widthMultiplier : CGFloat =  1.0
    var heightMultiplier : CGFloat = 1.0
    var direction : ShowDirection = .left

    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        
        let bounds = containerView!.bounds
        
        let xOffset = (direction == .left) ? 0 : (bounds.width * (1.0 - widthMultiplier))
        
//        print(bounds)
//        print("bounds width = \(bounds.width * 0.8)")
//        print("frame width = \(containerView?.frame.width)")
        return CGRect(x: xOffset,
                      y: 0,
                      width: bounds.width * widthMultiplier,
                      height: bounds.height * heightMultiplier)
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        containerView?.addSubview(presentedView!)
    }
    
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
//        presentedView?.round
    }
    
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
            super.presentationTransitionDidEnd(completed)
            
            if completed {
                driver.direction = .dismiss
            }
        }
    

}



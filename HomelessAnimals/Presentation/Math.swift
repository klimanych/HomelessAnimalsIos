//
//  Math.swift
//  HomelessAnimals
//
//  Created by klimanych on 25.03.2021.
//

import UIKit

extension CGFloat { // Velocity value
    func projectedOffset(decelerationRate: UIScrollView.DecelerationRate) -> CGFloat {
        // Magic formula from WWDC
        let multiplier = 1 / (1 - decelerationRate.rawValue) / 1000
//        print("multiplier = \(multiplier)")
        return self * multiplier
    }
}

extension CGPoint {
    func projectedOffset(decelerationRate: UIScrollView.DecelerationRate) -> CGPoint {
        return CGPoint(x: x.projectedOffset(decelerationRate: decelerationRate),
                       y: y.projectedOffset(decelerationRate: decelerationRate))
    }
}

extension CGPoint {
    static func +(left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x,
                       y: left.y + right.y)
    }
}

extension UIPanGestureRecognizer {
    func projectedLocation(decelerationRate: UIScrollView.DecelerationRate) -> CGPoint {
//        print("velocity in view = \(velocity(in: view))")
        let velocityOffset = velocity(in: view).projectedOffset(decelerationRate: .normal)
//        print("velocity offset = \(velocityOffset)")
//        print("location iv view = \(location(in: view!))")
        
        var locationTest = location(in: view!)
        if(locationTest.x < 0) {
            locationTest.x = -1.0 * locationTest.x
        }
        
        let projectedLocation = locationTest + velocityOffset
//        print("projectedLocation = \(projectedLocation)")
        return projectedLocation
    }
}

//
//  SlideInPresentationManager.swift
//  HomelessAnimals
//
//  Created by klimanych on 23.03.2021.
//

import UIKit

class SlideInPresentationManager: NSObject {
    
    
    enum PresentationDirection {
      case left
      case top
      case right
      case bottom
    }
    
    var direction: PresentationDirection = .left
    

}


extension SlideInPresentationManager: UIViewControllerTransitioningDelegate {
    
}

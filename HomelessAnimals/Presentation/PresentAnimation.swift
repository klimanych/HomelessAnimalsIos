//
//  PresentAnimation.swift
//  HomelessAnimals
//
//  Created by klimanych on 23.03.2021.
//

import UIKit

class PresentAnimation: NSObject {
    
    var direction : ShowDirection = .left
    let duration: TimeInterval = 0.5

        private func animator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
            // transitionContext.view содержит всю нужную информацию, извлекаем её
            let to = transitionContext.view(forKey: .to)!
            let finalFrame = transitionContext.finalFrame(for: transitionContext.viewController(forKey: .to)!) // Тот самый фрейм, который мы задали в PresentationController
            // Смещаем контроллер за границу экрана
            
            let multiplayer = direction == .left ? CGFloat(-1.0) : CGFloat(1.0)
            
            to.frame = finalFrame.offsetBy(dx: (multiplayer * finalFrame.width), dy: 0)
            let animator = UIViewPropertyAnimator(duration: duration, curve: .easeInOut) {
                to.frame = finalFrame // Возвращаем на место, так он выезжает снизу
            }

            animator.addCompletion { (position) in
            // Завершаем переход, если он не был отменён
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }

            return animator
        }
}



extension PresentAnimation: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let animator = self.animator(using: transitionContext)
        animator.startAnimation()
    }

    func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        return self.animator(using: transitionContext)
    }
}

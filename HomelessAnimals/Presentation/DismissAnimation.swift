//
//  DismissAnimation.swift
//  HomelessAnimals
//
//  Created by klimanych on 23.03.2021.
//

import UIKit

class DismissAnimation: NSObject {
    
    
    var direction : ShowDirection = .left
    let duration: TimeInterval = 0.5

        private func animator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
            let from = transitionContext.view(forKey: .from)!
            let initialFrame = transitionContext.initialFrame(for: transitionContext.viewController(forKey: .from)!)
            
            let multiplayer = direction == .left ? CGFloat(-1.0) : CGFloat(1.0)

            let animator = UIViewPropertyAnimator(duration: duration, curve: .easeOut) {
                from.frame = initialFrame.offsetBy(dx: (multiplayer * initialFrame.width), dy: 0)
            }

            animator.addCompletion { (position) in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }

            return animator
        }
    
}


extension DismissAnimation: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let animator = self.animator(using: transitionContext)
        animator.startAnimation()
    }

    func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        return self.animator(using: transitionContext)
    }
}

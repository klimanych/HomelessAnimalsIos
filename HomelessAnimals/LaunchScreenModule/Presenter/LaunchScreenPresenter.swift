//
//  LaunchScreenPresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 20.04.2021.
//

import Foundation


protocol LaunchScreenViewProtocol: class {
    func startAnimation()
}

protocol LaunchScreenViewPresenterProtocol: class {
    init(view: LaunchScreenViewProtocol, router: RouterProtocol)
    func moveToAuth()
}

class LaunchScreenPresenter: LaunchScreenViewPresenterProtocol {
    
    weak var view: LaunchScreenViewProtocol?
    var router: RouterProtocol
    
    required init(view: LaunchScreenViewProtocol, router: RouterProtocol) {
        self.view = view
        self.router = router
    }
    
    func moveToAuth() {
        router.authViewController()
    }
}

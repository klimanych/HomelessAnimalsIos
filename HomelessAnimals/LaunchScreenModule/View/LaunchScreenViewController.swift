//
//  LaunchScreenViewController.swift
//  HomelessAnimals
//
//  Created by klimanych on 20.04.2021.
//

import UIKit

class LaunchScreenViewController: UIViewController {
    
    var presenter: LaunchScreenViewPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.startAnimation()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LaunchScreenViewController: LaunchScreenViewProtocol {
    func startAnimation() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
            self.presenter.moveToAuth()
        })
        
//        Timer(timeInterval: 1.0, repeats: false, block: {_ in
//            self.presenter.moveToAuth()
//        })
    }
    
}

//
//  BasePresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 13.03.2021.
//

import Foundation


protocol BaseViewProtocol : class {
    func showError(text: String)
}


//
//  CreationPetStackButton.swift
//  HomelessAnimals
//
//  Created by klimanych on 16.03.2021.
//

import UIKit

class MapPetActionButton: UIButton {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        self.layer.backgroundColor = UIColor.white.cgColor
    }

}

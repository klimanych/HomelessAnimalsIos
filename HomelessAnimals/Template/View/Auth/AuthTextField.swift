//
//  AuthTextField.swift
//  HomelessAnimals
//
//  Created by klimanych on 21.04.2021.
//

import UIKit

@IBDesignable

class AuthTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override func draw(_ rect: CGRect) {
        
        
        let label = UILabel()
        
        
        self.addSubview(label)
        label.layer.masksToBounds = true
        label.frame.origin = self.bounds.origin
        self.sendSubviewToBack(label)
        label.frame.size = CGSize(width: 244, height: 45)
        label.backgroundColor = .white
        label.layer.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        label.layer.cornerRadius = 23
        label.layer.borderWidth = 2
        label.layer.borderColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1).cgColor
        label.center = self.convert(self.center, from: self.superview)
    
    }

}

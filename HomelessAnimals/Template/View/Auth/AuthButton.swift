//
//  AuthButton.swift
//  HomelessAnimals
//
//  Created by klimanych on 14.03.2021.
//

import UIKit

@IBDesignable

class AuthButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    override func draw(_ rect: CGRect) {
        
//        self.frame = CGRect(x: 0, y: 0, width: 92, height: 45)
        self.backgroundColor = .white
        self.layer.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        self.setTitle("Вход", for: .normal)
        self.setTitleColor(UIColor(red: 0, green: 0.478, blue: 1, alpha: 1), for: .normal)
        self.layer.cornerRadius = 23
    }

}

//
//  NetworkService.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import Foundation


protocol NetworkServiceProtocol {
    func getApiKey(email: String, password: String, completion: @escaping (Result<[String:String]?, Error>) -> Void)
    func validateApiKey(key: String, completion: @escaping (Result<[String:String]?,Error>) -> Void)
    func revokeApiKey(keyId: String, completion: @escaping (Result<[String:String]?,Error>) -> Void)
    func getPetsList(maxLatitude: Double, maxLongitude: Double, minLatitude: Double, minLongitude: Double, idArray: [Int], completion: @escaping (Result<[Pet],Error>) -> Void)
    func getConditionsList(completion: @escaping (Result<[Condition],Error>) -> Void)
    func getCategoriesList(completion: @escaping (Result<[Category],Error>) -> Void)
    func getStatusesList(completion: @escaping (Result<[Status],Error>) -> Void)
    func getAppealStatusesList(completion: @escaping (Result<[AppealStatus],Error>) -> Void)
    func getAppealTypesList(completion: @escaping (Result<[AppealType],Error>) -> Void)
    func createPetData(longitude: Double, latitude: Double, categoryId: Int, conditionId: Int, description: String, completion: @escaping (Result<Pet, Error>) -> Void)
    func updatePetData(longitude: Double, latitude: Double, categoryId: Int, conditionId: Int, description: String, petId: Int, completion: @escaping (Result<[String:String]?, Error>) -> Void)
    func getDevicesList(completion: @escaping (Result<[Token],Error>) -> Void)
}


class NetworkService : NetworkServiceProtocol {
    
    let localDataService: LocalDataServiceProtocol!
    let apiRoute = "http://192.168.88.133:8070/api"
    let deviceName : String!
    
    required init (localDataService: LocalDataServiceProtocol, deviceName: String) {
        self.localDataService = localDataService
        self.deviceName = deviceName
    }
    
    func getApiKey(email: String, password: String, completion: @escaping (Result<[String:String]?, Error>) -> Void) {
        
        let urlString = "\(self.apiRoute)/auth/sign_in"
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let authJson = [
            "email" : email,
            "password" : password,
            "device_name" : self.deviceName ?? "unknown device"
        ] as [String : Any]
        
        let authData = try! JSONSerialization.data(withJSONObject: authJson, options: [])
        
        URLSession.shared.uploadTask(with: request, from: authData) {data, _ , error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([String:String].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func validateApiKey(key: String, completion: @escaping (Result<[String:String]?,Error>) -> Void) {
        let urlString = "\(self.apiRoute)/auth/validate_key"
        guard let url = URL(string: urlString) else {return}
        
//        print("key in network service \(key)")
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let keyJson = [
            "token" : key
        ]
        
//        print(keyJson)
        
        let tokenData = try! JSONSerialization.data(withJSONObject: keyJson, options: [])
        
//        print("token data \(tokenData)")
        
        URLSession.shared.uploadTask(with: request, from: tokenData) {data, _, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([String:String].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func revokeApiKey(keyId: String, completion: @escaping (Result<[String : String]?, Error>) -> Void) {
        let urlString = "\(self.apiRoute)/auth/revoke_key"
        
        guard let url = URL(string: urlString) else {return}
        
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
//        print("key \(key)")
//        print("api_key_defaults \(apiKey)")
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        let keyData = [
            "tokenId" : keyId
        ]
        
        let tokenData = try! JSONSerialization.data(withJSONObject: keyData, options: [])
        
        URLSession.shared.uploadTask(with: request, from: tokenData) {data, _, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([String:String].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
        
    }
    
    
    func getPetsList(maxLatitude: Double, maxLongitude: Double, minLatitude: Double, minLongitude: Double, idArray: [Int], completion: @escaping (Result<[Pet], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/pets/list"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        
        
        let json = [
            "maxLatitude" : maxLatitude,
            "maxLongitude" : maxLongitude,
            "minLatitude" : minLatitude,
            "minLongitude" : minLongitude,
            "idArray" : idArray,
        ] as [String:Any]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        URLSession.shared.uploadTask(with: request, from: jsonData) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([Pet].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
        
    }
    
    func getCategoriesList(completion: @escaping (Result<[Category], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/categories/list"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([Category].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func getConditionsList(completion: @escaping (Result<[Condition], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/conditions/list"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([Condition].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    
    func getStatusesList(completion: @escaping (Result<[Status], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/statuses/list"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([Status].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func getAppealTypesList(completion: @escaping (Result<[AppealType], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/appeal_types/list"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([AppealType].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func getAppealStatusesList(completion: @escaping (Result<[AppealStatus], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/appeal_statuses/list"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([AppealStatus].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func getDevicesList(completion: @escaping (Result<[Token], Error>) -> Void) {
        let urlString = "\(self.apiRoute)/auth/devices"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([Token].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    
    func createPetData(longitude: Double, latitude: Double, categoryId: Int, conditionId: Int, description: String, completion: @escaping (Result<Pet, Error>) -> Void) {
        let urlString = "\(self.apiRoute)/pets/create"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        
        
        let json = [
            "longitude" : longitude,
            "latitude" : latitude,
            "categoryId" : categoryId,
            "conditionId" : conditionId,
            "description" : description,
        ] as [String:Any]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        URLSession.shared.uploadTask(with: request, from: jsonData) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode(Pet.self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    
    func updatePetData(longitude: Double, latitude: Double, categoryId: Int, conditionId: Int, description: String, petId: Int ,completion: @escaping (Result<[String:String]?, Error>) -> Void) {
        let urlString = "\(self.apiRoute)/pets/update"
        guard let url = URL(string: urlString) else {return}
        guard let apiKey = self.localDataService.getApiToken() else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        
        
        
        let json = [
            "longitude" : longitude,
            "latitude" : latitude,
            "category_id" : categoryId,
            "condition_id" : conditionId,
            "description" : description,
            "id" : petId
        ] as [String:Any]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        URLSession.shared.uploadTask(with: request, from: jsonData) {data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            do {
                let serverResponse = try JSONDecoder().decode([String:String].self, from: data!)
                completion(.success(serverResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    
}

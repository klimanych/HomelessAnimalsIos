//
//  LocalDataService.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import Foundation


protocol LocalDataServiceProtocol {
    func setValue(key: String, value: Any)
    func getValue(key: String) -> Any?
    func getPet(id: Int) -> Pet?
    func getPetsId() -> [Int]
    func appendPet(pet: Pet) -> Void
    func appendPet(petArray: [Pet]) -> Void
    func getCategory(id: Int) -> Category?
    func getCategories() -> [Category]
    func appendCategory(category: Category) -> Void
    func appendCategory(categoryArray: [Category]) -> Void
    func getCondition(id: Int) -> Condition?
    func getConditions() -> [Condition]
    func appendCondition(condition: Condition) -> Void
    func appendCondition(conditionArray: [Condition]) -> Void
    func getStatus(id: Int) -> Status?
    func getStatuses() -> [Status]
    func appendStatus(status: Status) -> Void
    func appendStatus(statusArray: [Status]) -> Void
    
    func getAppealStatus(id: Int) -> AppealStatus?
    func getAppealStatuses() -> [AppealStatus]
    func appendAppealStatus(appealStatus: AppealStatus) -> Void
    func appendAppealStatus(appealStatusArray: [AppealStatus]) -> Void
    
    func getAppealType(id: Int) -> AppealType?
    func getAppealTypes() -> [AppealType]
    func appendAppealType(appealType: AppealType) -> Void
    func appendAppealType(appealTypeArray: [AppealType]) -> Void
    
    
    func getApiToken() -> String?
    func getApiKey() -> String?
    func dropApiToken()
    func dropValue(key: String)
    func resetArrays()
}

class LocalDataService: LocalDataServiceProtocol {
    
    var defaults: UserDefaults
    var pets: [Pet]
    var categories: [Category]
    var conditions: [Condition]
    var statuses: [Status]
    var appealTypes : [AppealType]
    var appealStatuses: [AppealStatus]
    
    init() {
        self.defaults = UserDefaults.standard
        self.pets = [Pet]()
        
        
        let codedConditions = self.defaults.value(forKey: "conditions") as? Data
        let codedCategories = self.defaults.value(forKey: "categories") as? Data
        let codedStatuses = self.defaults.value(forKey: "statuses") as? Data
        let codedAppealStatuses = self.defaults.value(forKey: "appealStatuses") as? Data
        let codedAppealTypes = self.defaults.value(forKey: "appealTypes") as? Data
        
        if let codedConditions = codedConditions, let codedCategories = codedCategories, let codedStatuses = codedStatuses, let codedAppealTypes = codedAppealTypes, let codedAppealStatuses = codedAppealStatuses {
            let decodedConditions = try! JSONDecoder().decode([Condition].self, from: codedConditions)
            let decodedCategories = try! JSONDecoder().decode([Category].self, from: codedCategories)
            let decodedStatuses = try! JSONDecoder().decode([Status].self, from: codedStatuses)
            let decodedAppealStatuses = try! JSONDecoder().decode([AppealStatus].self, from: codedAppealStatuses)
            let decodedAppealTypes = try! JSONDecoder().decode([AppealType].self, from: codedAppealTypes)
            self.categories = decodedCategories
            self.conditions = decodedConditions
            self.statuses = decodedStatuses
            self.appealTypes = decodedAppealTypes
            self.appealStatuses = decodedAppealStatuses
        } else
        {
            self.categories = [Category]()
            self.conditions = [Condition]()
            self.statuses = [Status]()
            self.appealTypes = [AppealType]()
            self.appealStatuses = [AppealStatus]()
        }
    }
    
    func setValue(key: String, value: Any) {
        defaults.setValue(value, forKey: key)
    }
    
    func getValue(key: String) -> Any? {
        return defaults.value(forKey: key)
    }
    
    
    func getPet(id: Int) -> Pet? {
        return self.pets.first(where: {$0.id == id})
    }
    
    func getPetsId() -> [Int] {
        return self.pets.map {$0.id}
    }
    
    func appendPet(pet: Pet) {
        self.pets.append(pet)
    }
    
    func appendPet(petArray: [Pet]) {
        self.pets.append(contentsOf: petArray)
    }
    
    func getCategory(id: Int) -> Category? {
        return self.categories.first(where: {$0.id == id})
    }
    
    func getCategories() -> [Category] {
        return self.categories
    }
    
    func appendCategory(category: Category) {
        self.categories.append(category)
    }
    
    func appendCategory(categoryArray: [Category]) {
        self.categories.append(contentsOf: categoryArray)
    }
    
    func getCondition(id: Int) -> Condition? {
        return self.conditions.first(where: {$0.id == id})
    }
    
    func getConditions() -> [Condition] {
        return self.conditions
    }
    
    func appendCondition(condition: Condition) {
        self.conditions.append(condition)
    }
    
    func appendCondition(conditionArray: [Condition]) {
        self.conditions.append(contentsOf: conditionArray)
    }
    
    
    func getStatus(id: Int) -> Status? {
        return self.statuses.first(where: {$0.id == id})
    }
    
    func getStatuses() -> [Status] {
        return self.statuses
    }
    
    func appendStatus(status: Status) {
        self.statuses.append(status)
    }
    
    func appendStatus(statusArray: [Status]) {
        self.statuses.append(contentsOf: statusArray)
    }
    
    
    func getAppealType(id: Int) -> AppealType? {
        return self.appealTypes.first(where: {$0.id == id})
    }
    
    func getAppealTypes() -> [AppealType] {
        return self.appealTypes
    }
    
    func appendAppealType(appealType: AppealType) {
        self.appealTypes.append(appealType)
    }
    
    func appendAppealType(appealTypeArray: [AppealType]) {
        self.appealTypes.append(contentsOf: appealTypeArray)
    }
    
    func getAppealStatus(id: Int) -> AppealStatus? {
        return self.appealStatuses.first(where: {$0.id == id})
    }
    
    func getAppealStatuses() -> [AppealStatus] {
        return self.appealStatuses
    }
    
    func appendAppealStatus(appealStatus: AppealStatus) {
        self.appealStatuses.append(appealStatus)
    }
    
    func appendAppealStatus(appealStatusArray: [AppealStatus]) {
        self.appealStatuses.append(contentsOf: appealStatusArray)
    }
    
    
    
    func getApiToken() -> String? {
        return self.getValue(key: "api_token") as! String?
    }
    
    func getApiKey() -> String? {
        return self.getValue(key: "api_key") as! String?
    }
    
    func dropValue(key: String) {
        self.defaults.removeObject(forKey: key)
    }
    
    func dropApiToken() {
        self.dropValue(key: "api_token")
    }
    
    func resetArrays() {
        let codedConditions = self.defaults.value(forKey: "conditions") as? Data
        let codedCategories = self.defaults.value(forKey: "categories") as? Data
        let codedStatuses = self.defaults.value(forKey: "statuses") as? Data
        let codedAppealStatuses = self.defaults.value(forKey: "appealStatuses") as? Data
        let codedAppealTypes = self.defaults.value(forKey: "appealTypes") as? Data
        
        if let codedConditions = codedConditions, let codedCategories = codedCategories, let codedStatuses = codedStatuses, let codedAppealTypes = codedAppealTypes, let codedAppealStatuses = codedAppealStatuses {
            let decodedConditions = try! JSONDecoder().decode([Condition].self, from: codedConditions)
            let decodedCategories = try! JSONDecoder().decode([Category].self, from: codedCategories)
            let decodedStatuses = try! JSONDecoder().decode([Status].self, from: codedStatuses)
            let decodedAppealStatuses = try! JSONDecoder().decode([AppealStatus].self, from: codedAppealStatuses)
            let decodedAppealTypes = try! JSONDecoder().decode([AppealType].self, from: codedAppealTypes)
            self.categories = decodedCategories
            self.conditions = decodedConditions
            self.statuses = decodedStatuses
            self.appealTypes = decodedAppealTypes
            self.appealStatuses = decodedAppealStatuses
        } else
        {
            self.categories = [Category]()
            self.conditions = [Condition]()
            self.statuses = [Status]()
            self.appealTypes = [AppealType]()
            self.appealStatuses = [AppealStatus]()
        }
    }
    
}

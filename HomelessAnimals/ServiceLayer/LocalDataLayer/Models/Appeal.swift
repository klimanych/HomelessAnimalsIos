//
//  Appeal.swift
//  HomelessAnimals
//
//  Created by klimanych on 06.04.2021.
//

import Foundation


struct Appeal: Decodable {
    var id : Int
    var device: String
    var petId: Int
    var statusId: Int
    var typeId: Int
    var createdAt: String
}

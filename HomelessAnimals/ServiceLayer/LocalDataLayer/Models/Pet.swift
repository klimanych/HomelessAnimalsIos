//
//  Pet.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import Foundation


struct Pet: Decodable {
    var id : Int
    var latitude : Double
    var longitude : Double
    var description : String
    var categoryId : Int
    var conditionId : Int
    var statusId : Int
}

//
//  Condition.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import Foundation


struct Condition: Codable {
    var id : Int
    var name : String
    var color : String
}

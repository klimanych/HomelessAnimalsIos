//
//  Token.swift
//  HomelessAnimals
//
//  Created by klimanych on 08.04.2021.
//

import Foundation


struct Token: Decodable {
    var id : Int
    var name: String
    var createdAt: String
    var lastUsedAt: String

}

//
//  AppealType.swift
//  HomelessAnimals
//
//  Created by klimanych on 06.04.2021.
//

import Foundation


struct AppealType: Codable {
    var id : Int
    var name: String
}

//
//  AuthViewController.swift
//  HomelessAnimals
//
//  Created by klimanych on 09.03.2021.
//

import UIKit

class AuthViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    
    var presenter: AuthViewPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.checkApiKey()
    }

    @IBAction func AuthButtonPressed(_ sender: Any) {
        self.sendAuth()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        emailTextField.text = ""
        passwordTextField.text = ""
    }
}


extension AuthViewController : AuthViewProtocol {
    func sendAuth() {
        
        if let email = emailTextField.text, !email.isEmpty, let password = passwordTextField.text, !password.isEmpty {
            presenter.sendAuth(email: email, password: password)
            
        }
        else {
            self.showError(text: "credentials fields should be filled!!!")
        }
        
        
        
        
    }
    
    
    func checkedApi(result: Bool) {
//        print(result)
//        if !result {
//            authStackView.isHidden = false
//        }
    }
    
    func showError(text: String) {
        let alertController = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

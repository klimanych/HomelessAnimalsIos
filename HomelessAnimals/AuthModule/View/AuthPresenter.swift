//
//  AuthPresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 09.03.2021.
//

import Foundation


protocol AuthViewProtocol: BaseViewProtocol {
    func sendAuth()
    func checkedApi(result: Bool)
    
}

protocol AuthViewPresenterProtocol: class {
    init(view: AuthViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol)
    func sendAuth(email: String, password: String)
    func checkApiKey()
}

class AuthPresenter: AuthViewPresenterProtocol {
    
    weak var view: AuthViewProtocol?
    var router: RouterProtocol
    let networkService: NetworkServiceProtocol!
    let localDataService: LocalDataServiceProtocol!
    
    required init(view: AuthViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        self.localDataService = localDataService
    }
    
    func sendAuth(email: String, password: String) {
        
        //        print("email \(email) password \(password)")
        
        networkService.getApiKey(email: email, password: password) { [weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    //                    print(response)
                    
                    if let apiToken = response?["key"], let apiKey = response?["keyId"] {
                        self.localDataService.setValue(key: "api_token", value: apiToken)
                        self.localDataService.setValue(key: "api_key", value: apiKey)
                        
                        
                        let group = DispatchGroup()
                        group.enter()
                        self.networkService.getConditionsList() {[weak self] result in
                            switch result {
                            case .success(let response):
                                let encodedData: Data = try! JSONEncoder().encode(response)
                                //                                print(encodedData)
                                self?.localDataService.setValue(key: "conditions", value: encodedData)
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                            group.leave()
                        }
                        group.enter()
                        self.networkService.getCategoriesList() {[weak self] result in
                            switch result {
                            case .success(let response):
                                let encodedData: Data = try! JSONEncoder().encode(response)
                                self?.localDataService.setValue(key: "categories", value: encodedData)
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                            group.leave()
                        }
                        group.enter()
                        self.networkService.getAppealStatusesList() {[weak self] result in
                            switch result {
                            case .success(let response):
                                let encodedData: Data = try! JSONEncoder().encode(response)
                                self?.localDataService.setValue(key: "appealStatuses", value: encodedData)
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                            group.leave()
                        }
                        group.enter()
                        self.networkService.getStatusesList() {[weak self] result in
                            switch result {
                            case .success(let response):
                                let encodedData: Data = try! JSONEncoder().encode(response)
                                self?.localDataService.setValue(key: "statuses", value: encodedData)
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                            group.leave()
                        }
                        group.enter()
                        self.networkService.getAppealTypesList() {[weak self] result in
                            switch result {
                            case .success(let response):
                                let encodedData: Data = try! JSONEncoder().encode(response)
                                self?.localDataService.setValue(key: "appealTypes", value: encodedData)
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                            group.leave()
                        }
                        group.notify(queue: .main) {
                            self.localDataService.resetArrays()
                            if self.localDataService.getCategories().count > 0, self.localDataService.getConditions().count > 0, self.localDataService.getStatuses().count > 0, self.localDataService.getAppealTypes().count > 0, self.localDataService.getAppealStatuses().count > 0 {
                                self.router.showMap()
                            } else
                            {
                                self.view?.showError(text: "У нас не удалось загрузить все данные, попробуйте снова")
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    guard let errorMessage = response?["message"] else {return}
                    self.view?.showError(text: errorMessage)
                    
                    
                case.failure(let error):
                    self.view?.showError(text: error.localizedDescription)
                //                    print(error.localizedDescription)
                //                    print(error)
                }
            }
        }
        
        
    }
    
    func checkApiKey() {
        //        print("checking api")
        
        if let apiKey = localDataService.getValue(key: "api_token") as? String {
            //            print("api key from local \(apiKey)")
            networkService.validateApiKey(key: apiKey) { [weak self] result in
                guard let self = self else {return}
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        guard let successField = response?["success"] else {return}
                        switch successField {
                        case "true":
                            self.router.showMap()
                            break
                        default:
                            self.view?.checkedApi(result: false)
                            guard let messageField = response?["message"] else {return}
                            self.view?.showError(text: messageField)
                            break
                        }
                    case .failure(let error):
                        self.view?.checkedApi(result: false)
                        self.view?.showError(text: error.localizedDescription)
                    //                        self.router.popToRoot()
                    //                        print(error.localizedDescription)
                    }
                    
                }
            }
        } else {
            //            print("no api key")
            self.view?.checkedApi(result: false)
        }
    }
}

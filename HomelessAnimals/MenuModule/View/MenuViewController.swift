//
//  MenuViewController.swift
//  HomelessAnimals
//
//  Created by klimanych on 19.03.2021.
//

import UIKit

class MenuViewController: UIViewController {
    
    
    var presenter: MenuViewPresenterProtocol!
    var customTransition: PanelTransition!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print(self.transitioningDelegate)
        
        self.transitioningDelegate = customTransition
        // Do any additional setup after loading the view.
        
//        var panGesture = UIPanGestureRecognizer(target: self, action: #selector(gesture))
//        panGesture.edges = .all
//        view.addGestureRecognizer(panGesture)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        print(self.view.gestureRecognizers)
    }
    
    
    
    @IBAction func devicesButtonPressed(_ sender: Any) {
        presenter.showDevices()
    }
    
    @IBAction func logOutButtonPressed(_ sender: Any) {
        presenter.logOut()
    }
}



extension MenuViewController: MenuViewProtocol {
    
}

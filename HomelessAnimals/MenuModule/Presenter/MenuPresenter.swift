//
//  MenuPresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 19.03.2021.
//

import Foundation


protocol MenuViewProtocol: class {
    
}

protocol MenuViewPresenterProtocol: class {
    init(view : MenuViewProtocol, networkService : NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol)
    func logOut()
    func showDevices()
}


class MenuPresenter: MenuViewPresenterProtocol {
    
    
    weak var view: MenuViewProtocol?
    var router: RouterProtocol
    let networkService: NetworkServiceProtocol!
    let localDataService: LocalDataServiceProtocol!
    
    required init(view: MenuViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        self.localDataService = localDataService
    }
    
    func logOut() {
        if let key = localDataService.getApiKey() {
            networkService.revokeApiKey(keyId: key) { [weak self] result in
                guard let self = self else {return}
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
//                        print(response)
                        self.localDataService.dropApiToken()
                        self.router.popToRoot()
                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
        
    }
    
    func showDevices() {
        router.showDevices()
    }
    
    
}

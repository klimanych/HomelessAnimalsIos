//
//  Router.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import UIKit
import YandexMapsMobile


protocol RouterMain {
    var navigationController: UINavigationController? {get set}
    var assemblyBuilder: AssemblyBuilderProtocol? {get set}
}


protocol RouterProtocol: RouterMain {
    func initialViewController()
    func authViewController()
    func showMap()
    func showDetail(pet: Pet? , coordinates: YMKPoint?)
    func showMenu()
    func showDevices()
    func popToRoot()
    func popToPrevious()
    
}

class Router : RouterProtocol {
    
    var navigationController: UINavigationController?
    var assemblyBuilder: AssemblyBuilderProtocol?
    
    
    init(navigationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
        self.navigationController = navigationController
        self.assemblyBuilder = assemblyBuilder
    }
    
    
    func initialViewController() {
        if let navigationController = navigationController {
            navigationController.setNavigationBarHidden(true, animated: true)
            guard let launchScreenViewController = assemblyBuilder?.createLaunchSreenModule(router: self) else {return}
            navigationController.viewControllers = [launchScreenViewController]
//            navigationController.setToolbarHidden(true, animated: true)
        }
    }
    
    
    func authViewController() {
        if let navigationController = navigationController {
            navigationController.setNavigationBarHidden(true, animated: true)
            guard let authViewController = assemblyBuilder?.createAuthModule(router: self) else {return}
            navigationController.viewControllers = [authViewController]
//            navigationController.setToolbarHidden(true, animated: true)
        }
    }
    
    func showMap() {
        if let navigationController = navigationController {
//            navigationController.setToolbarHidden(true, animated: true)
            guard let mapViewController = assemblyBuilder?.createMapModule(router: self) else {return}
            navigationController.viewControllers.append(mapViewController)
        }
    }
    
    func showDetail(pet: Pet? , coordinates: YMKPoint?) {
        if let navigationController = navigationController {
//            navigationController.setToolbarHidden(true, animated: true)
            guard let detailViewController = assemblyBuilder?.createDetailModule(router: self, pet: pet, coordinates: coordinates) else {return}
            navigationController.present(detailViewController, animated: true, completion: nil)
        }
    }
    
    func showMenu() {
        if let navigationController = navigationController {
            
            let transitionClass = PanelTransition()
            transitionClass.widthMultiplier = CGFloat(0.8)
            
//            print(transitionClass.direction)
            guard let menuViewController = assemblyBuilder?.createMenuModule(router: self, panelTransition: transitionClass) else {return}
            
            menuViewController.modalPresentationStyle = .custom
            menuViewController.transitioningDelegate = transitionClass
            navigationController.present(menuViewController,animated: true,completion: nil)
            
//            navigationController.addChild(menuViewController)
//            navigationController.show(menuViewController, sender: nil)
            
//            print(navigationController.children)
        
        }
    }
    
    func showDevices() {
        if let navigationController = navigationController {
            let transitionClass = PanelTransition()
            transitionClass.direction = .right
            guard let devicesViewController = assemblyBuilder?.createDevicesModule(router: self, panelTransition: transitionClass) else {return}
            guard let currentController = navigationController.presentedViewController else {return}
            
            
            devicesViewController.modalPresentationStyle = .custom
            devicesViewController.transitioningDelegate = transitionClass
            
            currentController.present(devicesViewController, animated: true, completion: nil)
            
//            currentController.dismiss(animated: true, completion: nil)
//            navigationController.viewControllers.append(devicesViewController)
        }
    }
    
    func popToRoot() {
        if let navigationController = navigationController {
            
//            navigationController.popViewController(animated: true)
            
//            print(navigationController.viewControllers)
            navigationController.viewControllers.last?.dismiss(animated: true) {
                navigationController.popToRootViewController(animated: true)
            }
//            print(navigationController.viewControllers.last?.presentedViewController?.presentedViewController)
//            navigationController.popToRootViewController(animated: true)
//            navigationController.dismiss(animated: true, completion: nil)
        }
    }
    
    func popToPrevious() {
        if let navigationController = navigationController {
            navigationController.viewControllers.popLast()
        }
    }
    
    
    
    
}

//
//  MapPresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import Foundation
import YandexMapsMobile


protocol MapViewProtocol: BaseViewProtocol {
    func drawPets(arrayPet: [Pet])
}


protocol MapViewPresenterProtocol: class {
    init(view : MapViewProtocol, networkService : NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol)
    func calculateClusterDetailedCamera(placemarks: [YMKPlacemarkMapObject]) -> YMKBoundingBox?
    func logOut()
    func getPetsInArea(visibleRegion: YMKVisibleRegion)
    func showDetailView(pet: Pet?, coordinates: YMKPoint?)
    func showMenu()
}

class MapPresenter: MapViewPresenterProtocol {
    
    weak var view: MapViewProtocol?
    var router: RouterProtocol
    let networkService: NetworkServiceProtocol!
    let localDataService: LocalDataServiceProtocol!
    
    required init(view: MapViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        self.localDataService = localDataService
    }
    
    func calculateClusterDetailedCamera(placemarks: [YMKPlacemarkMapObject]) -> YMKBoundingBox? {
        let latitudeArray = placemarks.map {$0.geometry.latitude}
        let longitudeArray = placemarks.map {$0.geometry.longitude}
        
        if let maxLongitude = longitudeArray.max() , let minLongitude = longitudeArray.min(), let maxLatitude = latitudeArray.max(), let minLatitude = latitudeArray.min() {
            let boundingBox = YMKBoundingBox(southWest: YMKPoint(latitude: minLatitude, longitude: minLongitude), northEast: YMKPoint(latitude: maxLatitude, longitude: maxLongitude))
            return boundingBox
        } else {
            return nil
        }
        
        
    }
    
    func logOut() {
        if let key = localDataService.getApiKey() {
            networkService.revokeApiKey(keyId: key) { [weak self] result in
                guard let self = self else {return}
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
//                        print("revoke success")self.localDataService.dropApiKey()
                        self.router.popToRoot()
//                        print(response)
                        self.localDataService.dropApiToken()
                        self.router.popToRoot()
                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
        
    }
    
    func getPetsInArea(visibleRegion: YMKVisibleRegion) {
        
        let array = [visibleRegion.topLeft, visibleRegion.topRight, visibleRegion.bottomLeft, visibleRegion.bottomRight]
        
        let arrayLatitude = [visibleRegion.topLeft.latitude, visibleRegion.topRight.latitude, visibleRegion.bottomLeft.latitude, visibleRegion.bottomRight.latitude]
        let arrayLongitude = [visibleRegion.topLeft.longitude, visibleRegion.topRight.longitude, visibleRegion.bottomLeft.longitude, visibleRegion.bottomRight.longitude]
        
        networkService.getPetsList(maxLatitude: arrayLatitude.max() ?? 0.0, maxLongitude: arrayLongitude.max() ?? 0.0, minLatitude: arrayLatitude.min() ?? 0.0, minLongitude: arrayLongitude.min() ?? 0.0, idArray: self.localDataService.getPetsId())  { [weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.view?.drawPets(arrayPet: response)
                    self.localDataService.appendPet(petArray: response)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func showDetailView(pet: Pet?, coordinates: YMKPoint?) {
        self.router.showDetail(pet: pet, coordinates: coordinates)
    }
    
    func showMenu() {
        self.router.showMenu()
    }
    
    
    
}

//
//  MapViewController.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import UIKit
import YandexMapsMobile

class MapViewController: UIViewController {
    
//    MARK: - Private Variables
    
    private let FONT_SIZE: CGFloat = 15
    //    отступы от текста
    private let MARGIN_SIZE: CGFloat = 3
    //    толщина обводки
    private let STROKE_SIZE: CGFloat = 3
    //    минимальное расстояние между точками в кластере
    private let clusterRadius: Double = 20
    //    минимальный зум
    private let clusterZoom: UInt = 20
    
//    MARK: - Variables
    
    var presenter: MapViewPresenterProtocol!
    var locationLayer: YMKUserLocationLayer?
    var globalClusterCollection: YMKClusterizedPlacemarkCollection?
    var createPetObject: YMKPlacemarkMapObject?
    
//    MARK: - IBOutlets
    
    @IBOutlet weak var mapView: YMKMapView!
    @IBOutlet weak var userLocationButton: UIButton!
    @IBOutlet weak var zoomButtonsStack: UIStackView!
    @IBOutlet weak var createPetButton: UIButton!
    @IBOutlet weak var creationButtonStack: UIStackView!
    @IBOutlet weak var menuButton: UIButton!
    
    
//    MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.createUserLocationLayer()
        self.setMapDefaults()
        
    }
    
    
//    MARK: - IBActions
    
    @IBAction func userLocationButtonPressed(_ sender: Any) {
        if let position = self.locationLayer?.cameraPosition() {
            self.setCameraPosition(position: position)
        }
    }
    @IBAction func zoomInButtonPressed(_ sender: Any) {
        let cameraPosition = mapView.mapWindow.map.cameraPosition
        mapView.mapWindow.map.move(with: YMKCameraPosition.init(target: cameraPosition.target, zoom: cameraPosition.zoom + 1, azimuth: 0, tilt: 0), animationType: YMKAnimation(type: YMKAnimationType.linear, duration: 0.5))
    }
    
    @IBAction func zoomOutButtonPressed(_ sender: Any) {
        let cameraPosition = mapView.mapWindow.map.cameraPosition
        mapView.mapWindow.map.move(with: YMKCameraPosition.init(target: cameraPosition.target, zoom: cameraPosition.zoom - 1, azimuth: 0, tilt: 0), animationType: YMKAnimation(type: YMKAnimationType.linear, duration: 0.5))
    }
    @IBAction func createPetButtonPressed(_ sender: Any) {
        let currentCameraPosition = mapView.mapWindow.map.cameraPosition
        
        mapView.mapWindow.map.move(
            with: YMKCameraPosition.init(target: currentCameraPosition.target, zoom: mapView.mapWindow.map.getMaxZoom() - 2, azimuth: 0, tilt: 0))
        
        let placemark = mapView.mapWindow.map.mapObjects.addEmptyPlacemark(with: mapView.mapWindow.map.cameraPosition.target)
        let image = UIImage(systemName: "mappin", withConfiguration:nil)
        if let imageIcon = image {
            placemark.setIconWith(imageIcon, style: YMKIconStyle(anchor: CGPoint(x: 0.5, y: 1) as NSValue, rotationType: nil, zIndex: nil, flat: nil, visible: nil, scale: 2, tappableArea: nil))
        }
        self.createPetObject = placemark
        self.createPetButton.isEnabled = false
        self.creationButtonStack.isHidden = false
    }
    
    
    @IBAction func cancelCreatePetButtonPressed(_ sender: Any) {
        mapView.mapWindow.map.mapObjects.remove(with: self.createPetObject!)
        self.createPetObject = nil
        self.creationButtonStack.isHidden = true
        self.createPetButton.isEnabled = true
    }
    
    
    @IBAction func confirmCreatePetButtonPressed(_ sender: Any) {
        let placemark = mapView.mapWindow.map.cameraPosition.target
        self.presenter.showDetailView(pet: nil, coordinates: placemark)
    }
    
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        self.presenter.showMenu()
    }
    
    
    
    
    
//    MARK: - local actions
    
    
    func setMapDefaults() {
        mapView.mapWindow.map.isRotateGesturesEnabled = false
        let defaultCameraPosition = YMKCameraPosition.init(target: YMKPoint(latitude: 55.751574, longitude: 37.573856), zoom: 10, azimuth: 0, tilt: 0)
        mapView.mapWindow.map.move(
            with: locationLayer?.cameraPosition() ?? defaultCameraPosition)
        mapView.mapWindow.map.mapObjects.addTapListener(with: self)
        mapView.mapWindow.map.addInputListener(with: self)
        mapView.mapWindow.map.addCameraListener(with: self)
        mapView.mapWindow.map.setMapLoadedListenerWith(self)
        
        let clusterColection = mapView.mapWindow.map.mapObjects.addClusterizedPlacemarkCollection(with: self)
        self.globalClusterCollection = clusterColection
    }
    
    func createUserLocationLayer() {
        let locationLayer = YMKMapKit.sharedInstance().createUserLocationLayer(with: mapView.mapWindow)
        locationLayer.setVisibleWithOn(true)
        locationLayer.isHeadingEnabled = false
        locationLayer.setObjectListenerWith(self)
        self.locationLayer = locationLayer
    }
    
    func setCameraPosition(position: YMKCameraPosition) {
        mapView.mapWindow.map.move(with: position)
    }
    
//    MARK: -
    
    
    
    
    
}

// MARK: - MapViewProtocol extension
extension MapViewController: MapViewProtocol {
    func showError(text: String) {
        let alertController = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func drawPets(arrayPet: [Pet]) {
        if arrayPet.count > 0, let clusterCollection = self.globalClusterCollection {
            for pet in arrayPet {
                let placemark = clusterCollection.addEmptyPlacemark(with: YMKPoint(latitude: pet.latitude, longitude: pet.longitude))
                placemark.userData = pet
                let image = UIImage(named: "pet_category_\(pet.categoryId)") ?? UIImage(named: "question_mark")!
                placemark.setIconWith(image, style: YMKIconStyle(anchor: nil, rotationType: nil, zIndex: nil, flat: nil, visible: nil, scale: 0.3, tappableArea: nil))
            }
            clusterCollection.clusterPlacemarks(withClusterRadius: clusterRadius, minZoom: clusterZoom)
        }
    }
}

// MARK: - Yandex map extenstions
// MARK: YMKMapObjectTapListener
extension MapViewController: YMKMapObjectTapListener {
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        print("tapped object,not cluster")
        
        return true
    }
    
    
}

// MARK: YMKMapCameraListener

extension MapViewController: YMKMapCameraListener {
    func onCameraPositionChanged(with map: YMKMap, cameraPosition: YMKCameraPosition, cameraUpdateReason: YMKCameraUpdateReason, finished: Bool) {
//        print(cameraUpdateReason.rawValue)
        
        if let createPetObject = self.createPetObject {
            createPetObject.geometry = cameraPosition.target
        }
        
        if finished {
            self.presenter.getPetsInArea(visibleRegion: map.visibleRegion)
        }
    }
    
    
}

// MARK: YMKMapInputListener

extension MapViewController: YMKMapInputListener {
    func onMapTap(with map: YMKMap, point: YMKPoint) {
    }
    
    func onMapLongTap(with map: YMKMap, point: YMKPoint) {
        
    }
    
    
}

// MARK: YMKUserLocationObjectListener

extension MapViewController: YMKUserLocationObjectListener {
    func onObjectAdded(with view: YMKUserLocationView) {
        
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {
        
    }
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {
        
    }
    
    
}

// MARK: YMKMapLoadedListener

extension MapViewController: YMKMapLoadedListener {
    func onMapLoaded(with statistics: YMKMapLoadStatistics) {
        self.userLocationButton.isHidden = false
        self.zoomButtonsStack.isHidden = false
        self.createPetButton.isHidden = false
        self.menuButton.isHidden = false
    }
    
    
}

// MARK: YMKClusterListener

extension MapViewController: YMKClusterListener {
    
    func clusterImage(_ clusterSize: UInt) -> UIImage {
        let scale = UIScreen.main.scale
        let text = (clusterSize as NSNumber).stringValue
        let font = UIFont.systemFont(ofSize: FONT_SIZE * scale)
        let size = text.size(withAttributes: [NSAttributedString.Key.font: font])
        let textRadius = sqrt(size.height * size.height + size.width * size.width) / 2
        let internalRadius = textRadius + MARGIN_SIZE * scale
        let externalRadius = internalRadius + STROKE_SIZE * scale
        let iconSize = CGSize(width: externalRadius * 2, height: externalRadius * 2)
        
        UIGraphicsBeginImageContext(iconSize)
        let ctx = UIGraphicsGetCurrentContext()!
        
        ctx.setFillColor(UIColor.red.cgColor)
        ctx.fillEllipse(in: CGRect(
                            origin: .zero,
                            size: CGSize(width: 2 * externalRadius, height: 2 * externalRadius)));
        
        ctx.setFillColor(UIColor.white.cgColor)
        ctx.fillEllipse(in: CGRect(
                            origin: CGPoint(x: externalRadius - internalRadius, y: externalRadius - internalRadius),
                            size: CGSize(width: 2 * internalRadius, height: 2 * internalRadius)));
        
        (text as NSString).draw(
            in: CGRect(
                origin: CGPoint(x: externalRadius - size.width / 2, y: externalRadius - size.height / 2),
                size: size),
            withAttributes: [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: UIColor.black])
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        return image
    }
    
    func onClusterAdded(with cluster: YMKCluster) {
        cluster.appearance.setIconWith(clusterImage(cluster.size))
        cluster.addClusterTapListener(with: self)
    }
    
    
    
    
}

// MARK: YMKClusterTapListener

extension MapViewController: YMKClusterTapListener {
    func onClusterTap(with cluster: YMKCluster) -> Bool {
        let boundigBox = self.presenter.calculateClusterDetailedCamera(placemarks: cluster.placemarks)
        if let boundingBox = boundigBox {
            let newCameraPosition = mapView.mapWindow.map.cameraPosition(with: boundingBox)
//            mapView.mapWindow.map.move(with:newCameraPosition)
            mapView.mapWindow.map.move(with: YMKCameraPosition(target: newCameraPosition.target, zoom: newCameraPosition.zoom - 0.5, azimuth: newCameraPosition.azimuth, tilt: newCameraPosition.tilt))
        }
        return true
    }
    
    
}

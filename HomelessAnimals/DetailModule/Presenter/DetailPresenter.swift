//
//  DetailPresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import Foundation
import YandexMapsMobile

protocol DetailViewProtocol: BaseViewProtocol {
    func setData(pet : Pet?, categories: [Category], conditions: [Condition])
    func sendData(response: String)
}


protocol DetailViewPresenterProtocol: class {
    init(view : DetailViewProtocol, networkService : NetworkServiceProtocol, router: RouterProtocol, pet: Pet?, coordinates: YMKPoint?, localDataService: LocalDataServiceProtocol)
    func setData()
    func sendData(categoryIndex: Int, conditionIndex: Int, descriptionText: String)
}

class DetailPresenter: DetailViewPresenterProtocol {
    
    weak var view: DetailViewProtocol?
    var router: RouterProtocol
    let networkService: NetworkServiceProtocol!
    var pet: Pet?
    var coordinates: YMKPoint?
    let localDataService: LocalDataServiceProtocol!
    
    required init(view: DetailViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol, pet: Pet?, coordinates: YMKPoint?, localDataService: LocalDataServiceProtocol) {
        self.view = view
        self.networkService = networkService
        self.pet = pet
        self.router = router
        self.coordinates = coordinates
        self.localDataService = localDataService
    }
    
    func setData() {
        self.view?.setData(pet: pet, categories: self.localDataService.getCategories(), conditions: self.localDataService.getConditions())
    }
    
    func sendData(categoryIndex: Int, conditionIndex: Int, descriptionText: String) {
        
        if let coordinates = self.coordinates {
            
            networkService.createPetData(longitude: coordinates.longitude, latitude: coordinates.longitude, categoryId: categoryIndex, conditionId: conditionIndex, description: descriptionText) { [weak self] result in
                guard let self = self else {return}
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        print(response)
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
            
        }
        
    }
    
    
}

//
//  DetailViewController.swift
//  HomelessAnimals
//
//  Created by klimanych on 10.03.2021.
//

import UIKit

class DetailViewController: UIViewController {
    
    
    var presenter  : DetailViewPresenterProtocol!
    var pet: Pet?
    var categories: [Category]?
    var conditions: [Condition]?
    var categoryIdSelected: Int?
    var conditionIdSelected: Int?
    
    
//    MARK: - IBOutlets
    @IBOutlet weak var descriptionTextView: DetailPetTextView!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var conditionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.setData()
        setObservers()
        setPickerViews()
        
        
    }
    
    func setObservers() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (nc) in
            if self.descriptionTextView.isFirstResponder {

                let userInfo = nc.userInfo
                let getKeyboardRect = (userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
                var difference = CGFloat(0)
                if let strangeHeight = self.view.window?.frame.maxY {
                    difference = self.view.frame.maxY - strangeHeight
                }
                difference += getKeyboardRect.minY - self.descriptionTextView.frame.maxY - 5
                if difference <= 0, self.view.frame.origin == CGPoint(x: 0, y: 0) {
                    self.view.window?.frame.origin = CGPoint(x: 0.0, y: difference)
                }
            } else {
                self.view.window?.frame.origin = CGPoint(x: 0.0, y: 0)
            }
        }
        //
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (nc) in
            if self.descriptionTextView.isFirstResponder {
                self.view.window?.frame.origin = CGPoint(x: 0.0, y: 0)
            }
        }
    }
    
    func setPickerViews() {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        categoryTextField.inputView = pickerView
        conditionTextField.inputView = pickerView
        categoryTextField.delegate = self
        conditionTextField.delegate = self
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.view.window?.frame.origin = CGPoint(x: 0.0, y: 0)
    }
    
//    MARK: - IBActions
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
        
        if let conditionIdSelected = self.conditionIdSelected, let categoryIdSelected = self.categoryIdSelected {
            self.presenter.sendData(categoryIndex: categoryIdSelected, conditionIndex: conditionIdSelected, descriptionText: self.descriptionTextView.text)
            self.view.isUserInteractionEnabled = false
        }
        
    }
    
}

extension DetailViewController : DetailViewProtocol {
    func sendData(response: String) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showError(text: String) {
        
    }
    
    func setData(pet: Pet?, categories: [Category], conditions: [Condition]) {
        self.pet = pet
        self.categories = categories
        self.conditions = conditions
        
    }
    
    
}

extension DetailViewController: UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if categoryTextField.isFirstResponder {
            return categories?.count ?? 1
        }
        else if conditionTextField.isFirstResponder {
            return conditions?.count ?? 1
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if categoryTextField.isFirstResponder {
            return categories?[row].name
        }
        else if conditionTextField.isFirstResponder {
            return conditions?[row].name
        } else {
            return ""
        }
        
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if categoryTextField.isFirstResponder {
            self.categoryTextField.text = categories?[row].name
            self.categoryIdSelected = categories?[row].id
            self.categoryTextField.resignFirstResponder()
        }
        else if conditionTextField.isFirstResponder {
            self.conditionTextField.text = conditions?[row].name
            self.conditionIdSelected = conditions?[row].id
            self.conditionTextField.resignFirstResponder()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let picker = textField.inputView as? UIPickerView {
            picker.reloadAllComponents()
//            picker.resignFirstResponder()
        }
    }
    
    
    
    
    
    
}

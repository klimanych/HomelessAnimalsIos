//
//  DevicesViewController.swift
//  HomelessAnimals
//
//  Created by klimanych on 11.04.2021.
//

import UIKit

class DevicesViewController: UIViewController {
    
    var presenter: DevicesViewPresenterProtocol!
    var customTransition: PanelTransition!
    var currentTokenId: String!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.transitioningDelegate = customTransition
        
        
        
//        tableView.register(DeviceTableViewCell.self, forCellReuseIdentifier: "DeviceTableViewCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backwardButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.setResources()
        self.refreshData()
    }
    
}


extension DevicesViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.devices?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceTableViewCell", for: indexPath) as! DeviceTableViewCell
        cell.cellDelegate = self
        if let device = presenter.devices?[indexPath.row] {
            cell.DeviceNameLabel.text = device.name
            cell.LastUsedDateLabel.text = device.lastUsedAt
            
            if String(device.id) == self.currentTokenId {
                cell.deleteDeviceButton.isHidden = true
                cell.deleteDeviceButton.isEnabled = false
            }
        }
//        let device = presenter.devices?[indexPath.row]
//        cell.DeviceNameLabel.text = device?.name
//        cell.LastUsedDateLabel.text = device?.lastUsedAt
    
        // Configure the cell...
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
}


extension DevicesViewController : DevicesViewProtocol {
    func refreshData() {
//        print("success")
        self.tableView.reloadData()
    }
    func removeRow(indexPath: IndexPath) {
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    func failure(error: Error) {
        print(error.localizedDescription)
        let alert = UIAlertController(title: "Error", message: "Something went wront, log out", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in
            self.presenter.logOut()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setResources() {
        tableView.register(UINib(nibName: "DeviceTableViewCell", bundle: nil), forCellReuseIdentifier: "DeviceTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
    }
    
    
}

extension DevicesViewController: DeviceTableCellDelegate {
    func deleteDevice(cell: DeviceTableViewCell, didTapped: UIButton?) {
        guard let indexPath = tableView.indexPath(for: cell) else {return}
        presenter.deleteDevice(index: indexPath)
//        view

    }
    
    
}

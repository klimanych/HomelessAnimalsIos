//
//  DeviceTableViewCell.swift
//  HomelessAnimals
//
//  Created by klimanych on 11.04.2021.
//

import UIKit


protocol DeviceTableCellDelegate: class {
    func deleteDevice(cell: DeviceTableViewCell, didTapped: UIButton?)
}


class DeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var DeviceNameLabel: UILabel!
    @IBOutlet weak var LastUsedDateLabel: UILabel!
    @IBOutlet weak var deleteDeviceButton: UIButton!
    
    weak var cellDelegate: DeviceTableCellDelegate?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func DeleteButtonPressed(_ sender: Any) {
        cellDelegate?.deleteDevice(cell: self, didTapped: sender as? UIButton)
    }
}

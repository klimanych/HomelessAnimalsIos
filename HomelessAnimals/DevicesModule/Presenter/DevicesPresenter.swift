//
//  DevicesPresenter.swift
//  HomelessAnimals
//
//  Created by klimanych on 08.04.2021.
//

import Foundation


protocol DevicesViewProtocol : class {
    func failure(error: Error)
    func setResources()
    func refreshData()
    func removeRow(indexPath: IndexPath)
}


protocol DevicesViewPresenterProtocol: class {
    init(view : DevicesViewProtocol, networkService : NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol)
    func getDevices()
    func deleteDevice(index: IndexPath)
    func logOut()
    var devices: [Token]? {get set}
}

class DevicesPresenter: DevicesViewPresenterProtocol {
    var devices: [Token]?
    
    
    
    weak var view: DevicesViewProtocol?
    var router: RouterProtocol
    let networkService: NetworkServiceProtocol!
    let localDataService: LocalDataServiceProtocol!
    
    required init(view: DevicesViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol, localDataService: LocalDataServiceProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        self.localDataService = localDataService
        getDevices()
    }
    
    func getDevices() {
        networkService.getDevicesList() { [weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                switch result {
                case .success(let devices):
                    self.devices = devices
                    self.view?.refreshData()
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
            
        }
    }
    
    func deleteDevice(index: IndexPath) {
        
        if let device = devices?[index.row] {
            
            networkService.revokeApiKey(keyId: String(device.id)) { [weak self] result in
                guard let self = self else {return}
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        self.devices?.remove(at: index.row)
                        self.view?.removeRow(indexPath: index)
                    case .failure(let error):
                        print("error")
                    }
                }
            }
//            devices?.remove(at: index.row)
//            self.view?.removeRow(indexPath: index)
            
        }
    }
    
    func logOut() {
        localDataService.dropApiToken()
        router.popToRoot()
    }
}
